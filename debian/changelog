python-qtconsole (5.6.1-4) unstable; urgency=medium

  * Upload to unstable

 -- Julian Gilbey <jdg@debian.org>  Sun, 16 Feb 2025 15:05:23 +0000

python-qtconsole (5.6.1-3) experimental; urgency=medium

  [ Stuart Prescott ]
  * Move PyQt5 dependencies to jupyter-qtconsole package (Closes: #1089946).

  [ Julian Gilbey ]
  * Run autopkgtests on each Qt implementation (see discussion in #1089946)

 -- Julian Gilbey <jdg@debian.org>  Fri, 17 Jan 2025 11:00:39 +0000

python-qtconsole (5.6.1-2) unstable; urgency=medium

  * Explicitly depend on relevant PyQt5 libraries (closes: #1089946)

 -- Julian Gilbey <jdg@debian.org>  Wed, 25 Dec 2024 15:38:31 +0000

python-qtconsole (5.6.1-1) unstable; urgency=medium

  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Wed, 20 Nov 2024 18:41:46 +0000

python-qtconsole (5.5.2-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 5.5.2
  * Remove build-dependency on python3-ipython-genutils

 -- Alexandre Detiste <tchet@debian.org>  Sat, 03 Aug 2024 21:59:33 +0200

python-qtconsole (5.5.1-1) unstable; urgency=medium

  * New upstream version
  * Clean generated file in debian/clean (closes: #1046320)

 -- Julian Gilbey <jdg@debian.org>  Tue, 28 Nov 2023 07:18:25 +0000

python-qtconsole (5.4.3-2) unstable; urgency=medium

  * Run test_inprocess_kernel.py under unittest rather than pytest, as it
    causes pytest to crash inexplicably

 -- Julian Gilbey <jdg@debian.org>  Sun, 02 Jul 2023 23:16:43 +0100

python-qtconsole (5.4.3-1) unstable; urgency=medium

  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Tue, 27 Jun 2023 22:14:42 +0100

python-qtconsole (5.4.0-1) unstable; urgency=medium

  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Sat, 12 Nov 2022 21:56:10 +0000

python-qtconsole (5.3.2-1) unstable; urgency=medium

  * New upstream version
  * Add lintian override for screenshot lacking source

 -- Julian Gilbey <jdg@debian.org>  Wed, 31 Aug 2022 08:07:39 +0100

python-qtconsole (5.3.0-3) unstable; urgency=medium

  * Make jupyter-qtconsole depend on versioned python3-qtconsole
    (closes: #987362)
  * Add Rules-Requires-Root to d/control
  * Simplify autopkgtest script
  * Update lintian-overrides file
  * Add examples to doc package

 -- Julian Gilbey <jdg@debian.org>  Thu, 14 Jul 2022 09:35:40 +0100

python-qtconsole (5.3.0-2) unstable; urgency=medium

  * Improve autopkgtest suite: cover more tests
  * Do not install tests with package or run tests at build time
  * Do not build -doc package if nodoc profile is used

 -- Julian Gilbey <jdg@debian.org>  Tue, 26 Apr 2022 06:15:51 +0100

python-qtconsole (5.3.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Tue, 05 Apr 2022 13:04:55 +0100

python-qtconsole (5.2.2-1) unstable; urgency=medium

  [ Julian Gilbey ]
  * New upstream release.
  * Skip another breaking test :-(

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 5.2.2
  * wrap-and-sort -ast
  * d/pathes: Removed all patches (upstreamed)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 06 Jan 2022 14:30:40 +0100

python-qtconsole (5.1.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * New upstream release.

  [Picca Frédéric-Emmanuel]
  * d/control: Added dh-sequence-sphinxdoc
  * d/rules: Simplify thanks to dh-sequence-sphinxdoc.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 05 Sep 2021 20:32:16 +0200

python-qtconsole (5.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 21 Aug 2021 12:46:10 +0100

python-qtconsole (5.0.2-2) unstable; urgency=medium

  * Team upload.
  * Fix Build-Depends.

 -- Julian Gilbey <jdg@debian.org>  Fri, 05 Feb 2021 10:06:46 +0000

python-qtconsole (5.0.2-1) unstable; urgency=medium

  [ Julian Gilbey ]
  * Team upload.
  * New upstream version.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Julian Gilbey <jdg@debian.org>  Thu, 04 Feb 2021 23:20:41 +0000

python-qtconsole (4.7.6-1) unstable; urgency=medium

  [Debian Janitor]
  * Use secure URI in Homepage field.
  * Update standards version to 4.5.0, no changes needed.

  [Picca Frédéric-Emmanuel]
  * New upstream version 4.7.6

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 24 Aug 2020 16:50:34 +0200

python-qtconsole (4.7.4-1) unstable; urgency=medium

  * New upstream version 4.7.4

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 16 May 2020 14:51:38 +0200

python-qtconsole (4.7.2-1) unstable; urgency=medium

  * New upstream version 4.7.2
  * d/rules: do not call xvfb-run directly
  * d/control: Added python3-ipython (>= 7) (Closes: #953232)
  * d/control: Build-Depends: Added python3-zmq, python3-qtpy, python3-flaky
  * d/control: Build-Depends: Removed python3-pyqt5, python3-qpyqt5.qtsvg
  * d/t/control: Added python3-flaky
  * d/t/control: Allow stderr during test.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 29 Mar 2020 19:42:42 +0200

python-qtconsole (4.6.0-1) unstable; urgency=medium

  * New upstream version 4.6.0
  * d/patches: Removed and equivalent was upstreamed.
  * d/control:
    - Added Build-Depends, python3-pytestqt, python3-pytest-xvfb.
    - Removed Build-Depends, nose
  * d/rules: do not run test for now during the build.
  * d/t/control: Added to run the test with autopkgtests.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 09 Feb 2020 13:29:37 +0100

python-qtconsole (4.3.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Picca Frédéric-Emmanuel ]
  * Switched to compat level 12.
  * d/salsa-ci.yml: Created
  * Removed Python2 module (Closes: #938116)
  * Removed transitional ipython[3]-qtconsole (Closes: #940770, #940771)
  * d/control: Added Build-Depends python3-sphinx-rtd-theme

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 05 Dec 2019 09:54:27 +0100

python-qtconsole (4.3.1-1) unstable; urgency=medium

  * New upstream version 4.3.1 (Closes: #852308)
  * Bump standards-Version to 4.1.1 (nothing to do)
  * Switch to compat level 10

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 08 Oct 2017 12:16:47 +0200

python-qtconsole (4.2.1-3) unstable; urgency=medium

  * d/control
    - Added python-pyqt5, python-pyqt5.qtsvg,
      python3-pyqt5 and python3-pyqt5.qtsvg
      dependencies (Closes: #844620)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 03 Dec 2016 08:19:33 +0100

python-qtconsole (4.2.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 30 Oct 2016 20:36:23 +0100

python-qtconsole (4.2.1-2~exp3) experimental; urgency=medium

  * Still working on ... #830583

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 24 Aug 2016 20:36:23 +0200

python-qtconsole (4.2.1-2~exp2) experimental; urgency=medium

  * fix for real #830583

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 23 Aug 2016 22:59:24 +0200

python-qtconsole (4.2.1-2~exp1) experimental; urgency=medium

  * Add maintscript in order to (Closes: #830583)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 20 Aug 2016 18:29:37 +0200

python-qtconsole (4.2.1-1) experimental; urgency=low

  * Initial release (closes: #826456)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 22 May 2016 17:31:38 +0200
